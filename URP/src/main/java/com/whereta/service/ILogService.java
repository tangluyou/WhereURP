package com.whereta.service;

import com.whereta.vo.ResultVO;

import java.util.Map;

/**
 * Created by vincent on 15-9-12.
 */
public interface ILogService {

    ResultVO queryLoginLog(int page,int count);
    Map<String,Object> getAllUserLocations();


}
