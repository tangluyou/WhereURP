package com.whereta.mapper;

import com.whereta.model.Role;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("roleMapper")
public interface RoleMapper {
    /**
     * 根据主键删除
     * 参数:主键
     * 返回:删除个数
     * @ibatorgenerated 2015-08-27 16:17:57
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 插入，空属性也会插入
     * 参数:pojo对象
     * 返回:删除个数
     * @ibatorgenerated 2015-08-27 16:17:57
     */
    int insert(Role record);

    /**
     * 插入，空属性不会插入
     * 参数:pojo对象
     * 返回:删除个数
     * @ibatorgenerated 2015-08-27 16:17:57
     */
    int insertSelective(Role record);

    /**
     * 根据主键查询
     * 参数:查询条件,主键值
     * 返回:对象
     * @ibatorgenerated 2015-08-27 16:17:57
     */
    Role selectByPrimaryKey(Integer id);

    /**
     * 根据主键修改，空值条件不会修改成null
     * 参数:1.要修改成的值
     * 返回:成功修改个数
     * @ibatorgenerated 2015-08-27 16:17:57
     */
    int updateByPrimaryKeySelective(Role record);

    /**
     * 根据主键修改，空值条件会修改成null
     * 参数:1.要修改成的值
     * 返回:成功修改个数
     * @ibatorgenerated 2015-08-27 16:17:57
     */
    int updateByPrimaryKey(Role record);

    List<Role> selectAll();
}