package com.whereta.model;

import java.io.Serializable;

public class AccountRole implements Serializable {
    /**
     * account_role.id (用户角色id)
     * @ibatorgenerated 2015-08-27 16:17:57
     */
    private Integer id;

    /**
     * account_role.account_id (账号id)
     * @ibatorgenerated 2015-08-27 16:17:57
     */
    private Integer accountId;

    /**
     * account_role.role_id (角色id)
     * @ibatorgenerated 2015-08-27 16:17:57
     */
    private Integer roleId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }
}