define(function (require, exports, module) {
    var base = require('base');
    // 通过 require 引入依赖
    var F = module.exports = {
        basepath: '',
        jsSelectors:{
            dg:$('#main-user-manage-tb')
        },
        init: function (_basepath) {
            F.basepath = _basepath;

            F.jsSelectors.dg.datagrid({
                url: F.basepath+"/user/get-manage-user.html",
                fitColumns:true,
                idField:'id',
                striped:true,
                pagination:true,
                rownumbers:true,
                singleSelect:true,
                loadFilter:function(data){
                  if(data.ok){
                      return data.data;
                  }
                },
                onLoadError: function (arg) {
                    base.loadError(arg);
                },
                onLoadSuccess: function (data) {
                    $('.easyui-linkbutton').linkbutton();
                },
                columns:[[
                    {field:'id',title:'账号id',width:100},
                    {field:'account',title:'账号',width:100},
                    {field:'dep_name',title:'部门名字',width:100},
                    {field:'roleNames',title:'角色名字',width:100},
                    {field:'action',title:'操作',width:100, formatter: function (value, row, index) {
                        var action="";
                        action+="<a href='' plain='true' class='easyui-linkbutton' iconCls='icon-edit'>修改角色</a>";
                        return action;
                    }}
                ]]
            });
        }
    }
}
);