<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<html>
<head>
    <title>管理后台</title>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/assets/easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/assets/easyui/themes/icon.css">
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/assets/jquery/jquery.min.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/assets/easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/assets/easyui/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/assets/seajs/sea.js"></script>

    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/assets/icons/icons.css">

   <%-- <script type="text/javascript"  src="http://api.map.baidu.com/api?v=2.0&ak=cTYtHPV1spdnWgFSeKfXqjTh"></script>--%>
    <script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=cTYtHPV1spdnWgFSeKfXqjTh"></script>
</head>

<body class="easyui-layout">
<div data-options="region:'north',title:'',split:false,collapsible:false,border:false" style="height:100px;">
    <div style="margin-top: 30px;padding-left: 50px;">
        <span style="font-size: 20px;">WhereTa权限管理系统</span>
        <a href="${pageContext.request.contextPath}/user/exit.html" class="easyui-linkbutton" iconCls="icon-tuichu"
           style="float: right;margin-right: 50px;" plain="true">退出</a>
    </div>
    <jsp:include page="../../user-permission.jsp"></jsp:include>
</div>
<div data-options="region:'west',title:'导航',split:false,iconCls:'icon-daohang'" style="width:200px;padding: 5px;">
    <ul id="navigation-menu"></ul>
</div>
<div data-options="region:'center',title:''">
    <div id="center-content" class="easyui-tabs" fit="true" plain="true">
        <div title="欢迎页" iconCls="icon-huanying" style="padding: 5px;">
            欢迎页面
        </div>
    </div>
</div>
</body>
</html>
<script type="text/javascript">
    seajs.config({
        base: "${pageContext.request.contextPath}/assets/js/",
        alias: {}
    });
    //加载首页
    seajs.use(['base', 'main/index'], function (base, index) {
        if ($('#basejs_permission_del').length > 0) {
            base.perList.permission.del = true;
        }
        if ($('#basejs_permission_edit').length > 0) {
            base.perList.permission.edit = true;
        }
        if ($('#basejs_permission_create').length > 0) {
            base.perList.permission.create = true;
        }
        if ($('#basejs_permission_check').length > 0) {
            base.perList.permission.check = true;
        }
        if ($('#basejs_menu_edit').length > 0) {
            base.perList.menu.edit = true;
        }
        if ($('#basejs_menu_del').length > 0) {
            base.perList.menu.del = true;
        }
        if ($('#basejs_menu_create').length > 0) {
            base.perList.menu.create = true;
        }
        if ($('#basejs_role_create').length > 0) {
            base.perList.role.create = true;
        }
        if ($('#basejs_role_del').length > 0) {
            base.perList.role.del = true;
        }
        if ($('#basejs_role_edit').length > 0) {
            base.perList.role.edit = true;
        }
        if ($('#basejs_department_create').length > 0) {
            base.perList.department.create = true;
        }
        if ($('#basejs_department_del').length > 0) {
            base.perList.department.del = true;
        }
        if ($('#basejs_department_edit').length > 0) {
            base.perList.department.edit = true;
        }
        if ($('#basejs_role_grant').length > 0) {
            base.perList.role.grant = true;
        }
        if ($('#basejs_role_permission_check').length > 0) {
            base.perList.role.checkPermission = true;
        }
        if ($('#basejs_role_check').length > 0) {
            base.perList.role.check = true;
        }
        if ($('#basejs_menu_check').length > 0) {
            base.perList.menu.check = true;
        }
        if ($('#basejs_menu_permission_check').length > 0) {
            base.perList.menu.checkPermission = true;
        }
        if ($('#basejs_menu_grant').length > 0) {
            base.perList.menu.grant = true;
        }
        index.init('${pageContext.request.contextPath}');
    });
</script>
