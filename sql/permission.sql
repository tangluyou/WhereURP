/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : urp

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2015-09-02 14:23:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission` (
  `id` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT COMMENT '权限id',
  `name` varchar(255) NOT NULL COMMENT '权限名字',
  `key` varchar(255) NOT NULL COMMENT '权限key',
  `parent_id` int(10) DEFAULT NULL COMMENT '上级权限',
  `order` int(11) NOT NULL DEFAULT '0' COMMENT '权限排序',
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of permission
-- ----------------------------
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (1, '权限', '*', null, 0);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (2, '系统设置', 'system:setting', 1, 10);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (3, '菜单设置', 'menu', 2, 0);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (4, '角色设置', 'role', 2, 0);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (5, '权限设置', 'permission', 2, 0);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (6, '查看权限', 'permission:check', 5, 0);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (7, '删除权限', 'permission:delete', 5, 0);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (8, '添加权限', 'permission:add', 5, 0);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (9, '编辑权限', 'permission:edit', 5, 0);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (11, '编辑菜单', 'menu:edit', 3, 0);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (12, '删除菜单', 'menu:delete', 3, 0);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (13, '添加菜单', 'menu:add', 3, 0);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (14, '用户设置', 'user', 18, 5);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (15, '部门设置', 'dep', 18, 1);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (16, '查看菜单', 'menu:check', 3, 0);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (17, '菜单授权', 'menu:grant', 3, 0);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (18, '用户部门', 'userdep', 1, 0);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (19, '创建部门', 'dep:add', 15, 0);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (20, '删除部门', 'dep:delete', 15, 1);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (21, '编辑部门', 'dep:edit', 15, 2);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (22, '添加角色', 'role:add', 4, 1);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (23, '删除角色', 'role:delete', 4, 2);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (24, '编辑角色', 'role:edit', 4, 3);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (25, '权限管理(页面)', 'permission:manage', 5, 0);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (26, '查看角色', 'role:check', 4, 0);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (27, '角色授权', 'role:grant', 4, 4);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (28, '角色管理(页面)', 'role:manage', 4, 5);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (29, '查看部门', 'dep:check', 15, 3);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (30, '部门管理(页面)', 'dep:manage', 15, 4);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (31, '添加用户', 'user:add', 14, 0);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (32, '删除用户', 'user:delete', 14, 1);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (33, '编辑用户', 'user:edit', 14, 2);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (34, '查看用户', 'user:check', 14, 3);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (35, '用户管理(页面)', 'user:manage', 14, 4);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (36, '查看角色权限', 'rolepermission:check', 27, 0);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (37, '查询权限，不包括自己以及子级权限', 'permission:check:notselfchildren', 6, 1);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (38, '查询菜单权限', 'menupermisson:check', 17, 1);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (39, '菜单管理（页面）', 'menu:manage', 3, 1);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (40, '查看菜单，不包括自己以及子级菜单', 'menu:check:notselfchildren', 16, 1);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (41, '日志记录', 'logs', 1, 4);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (42, '登录日志（页面）', 'loginlog:manage', 41, 1);
INSERT INTO urp.permission (id, name, `key`, parent_id, `order`) VALUES (43, '登录日志查看', 'loginlog:check', 41, 2);
